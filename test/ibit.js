const axios = require("axios").default;
const cookiestr = "__guid_session=A875622C0846410082B9D9ABEB859BE9; Hm_lvt_182727065eae0d5c3927d699ca0b3717=1586229254; uid=0E7141FCBB124754AE289BDADA2CCBDD; Hm_lpvt_182727065eae0d5c3927d699ca0b3717=1586229259";

/**
 * 
 * @param {Array} ids 
 */
function deleteTask(ids) {
  return new Promise(resolve => {
    axios.get(`https://client.ibit6.com/mydisk/DelDiskFile`, {
      params: {
        ids: ids.join(",")
      },
      headers: {
        cookie: cookiestr
      },
      validateStatus: s => true
    }).then(axresp => {
      if (!(axresp.data
        && axresp.data['error'] == 0)) {
        console.log("删除失败", axresp.data)
      }
      resolve()
    })
  })
}

/**
 * @description status==1d的表示是解析中
 * @param {Number} start_pageindex 
 */
function getDiskInfo(start_pageindex) {
  return new Promise(resolve => {
    axios.get(`https://client.ibit6.com/mydisk/GetDiskInfo?start=${start_pageindex}`, {
      headers: {
        cookie: cookiestr
      }
    }).then(axresp => {
      resolve(axresp.data)
    })
  })
}

(async 获取所有文件 => {
  let files = []
  for (let i = 0; i <= 47; i++) {
    let o_diskinfo = await getDiskInfo(i);
    files = files.concat(o_diskinfo.files)
    // debugger
  }
  let path = require("path");
  let data = JSON.stringify(files)
  require("fs").writeFile(path.join(__dirname, "./ibit6_tasks.json"), data, (err) => {
    debugger
  })
  // debugger
});

(async delete_status1 => {
  let files = require("./ibit6_tasks.json");
  let status1 = files.filter(e => e.status == 1);
  await deleteTask(status1.map(e => e.id))
  debugger
})()