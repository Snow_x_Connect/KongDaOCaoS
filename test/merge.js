const libK = require("../lib").libKDocsDrive;
const path = require("path");
const fs = require("fs");
const FormData = require("form-data");
const Toolbox = require("../tool/toolbox");
const jexcel = require('json2excel');
const XLSX = require("xlsx");
const CombinedStream = require('combined-stream');

let cStream = CombinedStream.create();
let f1 = fs.createReadStream(path.join(__dirname, "../tmp/MHT.mht"), {
  start: 0,
  end: 1024 * 1024
});
let f2 = fs.createReadStream(path.join("D:\\TestMerge\\40个周报.part1.rar"));
cStream.append(f1);
cStream.append(f2);
let writeStream = fs.createWriteStream(path.join("D:\\TestMerge\\40个周报+mht.part1.rar"));
cStream.pipe(writeStream);
