#!/usr/bin/env node
const args = require("args");
const path = require("path");
const fs = require("fs");
const process = require("process");
const getUploadPaths = require("./get_upload_paths.js").getUploadPaths;
const KdocDrive = require("../lib/KdocsDrive").KdocsDrive;
const toWebhook = require("./to_worktile_daily_webhook");
const runParallel = require("run-parallel-limit");
let cwd = process.cwd()
console.log("cwd :", cwd);
let flags = args.parse(process.argv);
let sub = args.sub;

let wps = new KdocDrive('V02SM4K7yqLwAoUXAhCzMtMvNDDkGRQ00a10b3320029d3cb62');
const KDFILE_ROOT_FOLDER = 56283529888;
getUploadPaths(cwd, sub).then(async avai_paths => {
  let get_sg = await wps.GetSpecialGroupPromise;
  if (!get_sg.ok) {
    console.error(get_sg.msg);
    return process.exit();
  }
  let sg = get_sg.data.special_group;
  let tasks = avai_paths.map(ap => async cb => {
    if (ap.stats.isFile()) {
      let upppp = await sg.app.uploadFileAsSingleFolder(ap.full_path, KDFILE_ROOT_FOLDER);
      if (!upppp.ok) {
        await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.kdoc-fail`), `
        \n${ap.full_path}
        \n ERR = ${upppp.msg}
        `)
        // continue;
        return cb();
      }
      let content = `UPLOAD TO KDOC 超级会员号
      \n路径= ${markdownify(ap.full_path)}
      \n私有 = http://kd-file.liblu.me/2001/folder/${upppp.data.folder_id}/p/1#${upppp.data.folder_name}
      \n\`\`\`
      \n ${ap.full_path}
      \n PUBLIC : http://kd-file.liblu.me/2001/ps/folder/${upppp.data.folder_id}/p/1
      \n\`\`\`
      `;
      await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.kdoc-ok`), content);
      // await toWebhook.create_task(path.basename(ap.full_path), content);
      return cb();
    } else if (ap.stats.isDirectory()) {
      let upppp = await sg.app.uploadEntireFolder(ap.full_path, KDFILE_ROOT_FOLDER);
      if (!upppp.ok) {
        await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.kdoc-fail`), `
        \n${ap.full_path}
        \n ERR = ${upppp.msg}
        `)
        return cb();
      }
      let content = `UPLOAD TO KDOC 超级会员号
      \n路径= ${markdownify(ap.full_path)}
      \n私有 = http://kd-file.liblu.me/2001/folder/${upppp.data.folder_id}/p/1#${upppp.data.folder_name}
      \n\`\`\`
      \n ${ap.full_path}
      \n PUBLIC : http://kd-file.liblu.me/2001/ps/folder/${upppp.data.folder_id}/p/1
      \n\`\`\`
      `;
      await createTextFile(path.join(cwd, `${path.basename(ap.full_path)}.kdoc-ok`), content);
      // await toWebhook.create_task(path.basename(ap.full_path), content);
      return cb();
    }
  });
  runParallel(tasks, 3, () => {
    console.log("kdocs --done")
    process.exit(0);
  })

})


/**
 * @description 转义一般文本中的 []()\等特殊符号
 * @param {String} str 
 */
function markdownify(str) {
  str = str.replace(/\\/g, "\\\\");
  str = str.replace(/\[/g, "\\[");
  str = str.replace(/\]/g, "\\]");
  str = str.replace(/\(/g, "\\(");
  str = str.replace(/\)/g, "\\)");
  return str;
}

/**
 * @returns {Promise<{ok:Boolean,msg:String}>}
 * @param {String} file_fullpath 
 * @param {String} content 
 */
function createTextFile(file_fullpath, content) {
  return new Promise(async resolve => {
    fs.writeFile(file_fullpath, content, {
      flag: "w+",
      encoding: "utf8"
    }, (err) => {
      if (err) {
        return resolve({
          ok: false,
          msg: `${err.message}`
        })
      }
      resolve({
        ok: true,
        msg: "ok"
      })
    })
  })
}