const axios = require("axios").default;
const WEBHOOK_URL_FOR_DAILY_UPLOAD =
  "https://hook.worktile.com/project/incoming/b3295fc65b814ee2984d1f48f1a11979";
const CommonAxerrH = require("../tool/CommonAxerrHandlerGenerator").CommonAxerrHandlerGen;

/**
 * @returns {Promise<{ok:Boolean,msg:String}>}
 * @param {String} title 
 * @param {string} desc_html_string 
 */
function createTaskInner(title, desc_html_string) {
  return new Promise(resolve => {
    axios.post(WEBHOOK_URL_FOR_DAILY_UPLOAD, {
      action: "create_task",
      payload: {
        type: "KDOCS-FILE",
        title: `[KDOCS]${title}`,
        properties: {
          "desc": desc_html_string,
        }
      }
    }, {
      headers: {
        "Content-Type": "application/json"
      },
      validateStatus: s => s == 200
    }).then(axresp => {
      if (axresp.data && axresp.data.code == 200) {
        return resolve({
          ok: true,
          msg: "ok"
        })
      }
      return CommonAxerrH(resolve)(axresp.data)
    }).catch(axerr => {
      CommonAxerrH(resolve)(axerr);
    })
  })
}


/**
 * @returns {Promise<{ok:Boolean,msg:String}>}
 * @param {String} title 
 * @param {string} desc_html_string 
 */
function createTask(title, desc_html_string) {
  return new Promise(async resolve => {
    for (let i = 1; i <= 4; i++) {
      let o_try = await createTaskInner(title, desc_html_string);
      if (o_try.ok) {
        return resolve(o_try);
      }
      if (i == 4) {
        return resolve({
          ok: false,
          msg: `failed after multy try:${o_try.msg}`
        })
      } else {
        await new Promise(wait => setTimeout(wait, 1000))
      }
    }
  })
}


module.exports = {
  create_task:createTask
}